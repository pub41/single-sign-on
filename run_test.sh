#!/usr/bin/env bash

cd ./src
/usr/bin/composer install
./vendor/bin/phpunit --log-junit /tmp/phpunit-report.xml --coverage-clover /tmp/coverage.xml
exit 0